INSERT INTO `karyawan` VALUES 
    (1,'Rizki Saputra','L','Menikah','1980-11-10','2011-01-01',1),
    (2,'Farhan Reza','L','Belum','1989-01-11','2011-01-01',1),
    (3,'Riyando Adi','L','Menikah','1977-01-25','2011-01-01',1),
    (4,'Diego Manuel','L','Menikah','1983-02-22','2012-09-04',2),
    (5,'Satya Laksana','L','Menikah','1981-01-12','2011-03-19',2),
    (6,'Miguel Hernandez','L','Menikah','1994-10-16','2014-06-15',2),
    (7,'Putri Persada','P','Menikah','1988-01-30','2013-04-14',2),
    (8,'Alma Safira','P','Menikah','1991-05-18','2013-09-28',3),
    (9,'Haqi Hafiz','L','Belum','1995-09-19','2015-03-09',3),
    (10,'Abi Isyawara','L','Belum','1991-03-06','2012-01-22',3),
    (11,'Maman Kresna','L','Belum','1993-08-21','2012-09-15',3),
    (12,'Nadia Aulia','P','Belum','1989-07-10','2012-05-07',4),
    (13,'Mutiara Rezki','P','Menikah','1988-03-23','2013-05-21',4),
    (14,'Dani Setiawan','L','Belum','1986-02-11','2014-11-30',4),
    (15,'Budi Putra','L','Belum','1995-10-23','2015-12-03',4);


INSERT INTO `departemen` VALUES 
    (1,'ManaJemen'),(2,'Pengembangan Bisnis'),(3,'Teknisi'),(4,'Analis');
